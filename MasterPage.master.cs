﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Tools;

public partial class MasterPage : System.Web.UI.MasterPage
{
    public static String managerLink = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        LoginCheck();
        setManager();
    }
    private void LoginCheck()
    {
        if (Session["userID"]==null||!StringChecks.IsInt(Session["userID"]))
        {
            Response.Redirect("Login.aspx?ERROR=NO_SESSION");
        }

    }
    private void setManager()
    {
        if (StringChecks.IsInt(Session["isManager"], 1)){
            managerLink = "<li ><a href=\"Sell.aspx\">דף למנהל</a></li>";
        }
    }

    protected void SighnOut_Click(object sender, EventArgs e)
    {
        Session.Abandon();
        Response.Redirect("Login.aspx");
    }
}
