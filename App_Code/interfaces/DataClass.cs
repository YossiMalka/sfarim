﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for DataClass
/// </summary>
public interface DataClass
{
    void GetFromDB(String id);
    DataClass GetNewFromDB(String id);
    DataFather GetDBData(String id);
    DataFather GetDBData();

}