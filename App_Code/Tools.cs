﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Class1
/// </summary>
namespace Tools
{
    public static class StringChecks
    {
        public static bool IsInt(String str)
        {
            try
            {
                int.Parse(str);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        public static bool IsInt(Object str)
        {
            try
            {
                int.Parse(str.ToString());
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        public static bool IsInt(String str, int num)
        {
            try
            {
                int.Parse(str);
                return int.Parse(str) == num;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        public static bool IsInt(Object obj, int num)
        {
            try
            {
                String str = obj.ToString();
                int.Parse(str);
                return int.Parse(str) == num;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        public static bool IsFloat(String str)
        {
            try
            {
                float.Parse(str);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        public static bool IsFloat(Object str)
        {
            try
            {
                float.Parse(str.ToString());
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        public static bool IsFloat(String str, float num)
        {
            try
            {
                float.Parse(str);
                return float.Parse(str) == num;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        public static bool IsFloat(Object obj, float num)
        {
            try
            {
                String str = obj.ToString();
                float.Parse(str);
                return float.Parse(str) == num;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        public static bool IsDouble(String str)
        {
            try
            {
                double.Parse(str);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        public static bool IsDouble(Object str)
        {
            try
            {
                double.Parse(str.ToString());
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        public static bool IsDouble(String str, double num)
        {
            try
            {
                double.Parse(str);
                return double.Parse(str) == num;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        public static bool IsDouble(Object obj, double num)
        {
            try
            {
                String str = obj.ToString();
                double.Parse(str);
                return double.Parse(str) == num;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        public static bool IsLong(String str)
        {
            try
            {
                long.Parse(str);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public static bool IsLong(Object str)
        {
            try
            {
                long.Parse(str.ToString());
                return true;
            }
            catch
            {
                return false;
            }
        }
        public static bool IsLong(String str, long num)
        {
            try
            {
                long.Parse(str);
                return long.Parse(str) == num;
            }
            catch
            {
                return false;
            }
        }
        public static bool IsLong(Object obj, long num)
        {
            try
            {
                String str = obj.ToString();
                long.Parse(str);
                return long.Parse(str) == num;
            }
            catch
            {
                return false;
            }
        }
        public static bool IsChar(String str)
        {
            try
            {
                char.Parse(str);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        public static bool IsChar(Object str)
        {
            try
            {
                char.Parse(str.ToString());
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        public static bool IsChar(String str, char ch)
        {
            try
            {
                char.Parse(str);
                return char.Parse(str) == ch;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        public static bool IsChar(Object obj, char ch)
        {
            try
            {
                String str = obj.ToString();
                char.Parse(str);
                return char.Parse(str) == ch;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        public static bool IsBool(String str)
        {
            try
            {
                bool.Parse(str);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        public static bool IsBool(Object str)
        {
            try
            {
                bool.Parse(str.ToString());
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        public static bool IsBool(String str, bool condition)
        {
            try
            {
                bool.Parse(str);
                return bool.Parse(str) == condition;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        public static bool IsBool(Object obj, bool condition)
        {
            try
            {
                String str = obj.ToString();
                bool.Parse(str);
                return bool.Parse(str) == condition;
            }
            catch (Exception e)
            {
                return false;
            }
        }

    }
    public static class StringTools
    {
        public static String RewriteFrom(String str, String start)
        {
            if (IsSubstring(str, start))
            {
                return str.Substring(GetSubstringIndex(str, start));
            }
            return "";
        }
        public static bool IsSubstring(String str, String check)
        {
            int currentIndex = 0;
            for (int i = 0; i < str.Length; i++)
            {
                if (str[i] == check[currentIndex])
                {
                    if (currentIndex == check.Length - 1)
                    {
                        return true;
                    }
                    else
                    {
                        currentIndex++;
                    }
                }
                else
                {
                    currentIndex = 0;
                }
            }
            return false;
        }
        public static int GetSubstringIndex(String str, String check)
        {
            int currentIndex = 0;
            int substringIndex = 0;
            for (int i = 0; i < str.Length; i++)
            {
                if (str[i] == check[currentIndex])
                {
                    if (currentIndex == check.Length - 1)
                    {
                        return substringIndex;
                    }
                    else
                    {
                        if (currentIndex == 0)
                        {
                            substringIndex = i;
                        }
                        currentIndex++;
                    }
                }
                else
                {
                    currentIndex = 0;
                }
            }
            return -1;
        }
        public static void AddSlesh(String text)
        {
            String[] str = text.Split('"');
            for (int i = 1; i < str.Length; i++)
            {
                str[i] = "\\" + str;
            }
            text = "";
            foreach (String txt in str)
            {
                text += txt;
            }
        }

    }
}
