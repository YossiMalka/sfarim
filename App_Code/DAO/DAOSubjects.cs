﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

public class DAOSubjects : DAOBasic
{
    public DAOSubjects() { }
    public static String GetName(String id)
    {
        return GetUsingId(id, "SubjectName", DataTables.SUBJECTS);
    }
    public static Subject GetSubject(String id)
    {
        string name = GetUsingId(id, "SubjectName", DataTables.SUBJECTS);
        return new Subject(id, name);
    }
    public static void AddSubject(Subject sub)
    {
        SqlCommand command = new SqlCommand();
        command.Connection = Connection();
        command.Parameters.AddWithValue("@name", sub.name);
        command.CommandText = "INSERT INTO OFFERS (SubjectName) VALUES (@name)";
        command.Connection.Open();
        int rowsAffected = command.ExecuteNonQuery();
        command.Connection.Close();
    }
    public static void UpdateSubject(Subject sub)
    {
        Do("UPDATE OFFERS SET SubjectName='" + sub.name + "' WHERE id=" + sub.id, Connection());
    }
}
