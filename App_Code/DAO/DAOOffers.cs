﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for DAOOffers
/// </summary>
public class DAOOffers : DAOBasic
{
    public DAOOffers()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public static User GetUser(String id)
    {
        User usr = new User();
        usr.GetDBData(GetUsingId(id, "userID", "OFFERS", Connection()));
        return usr;
    }
    public static String GetPrice(String id)
    {
        return GetUsingId(id, "price", "OFFERS", Connection());
    }
    public static Book GetBook(String id)
    {
        Book bk = new Book();
        bk.GetDBData(GetUsingId(id, "bookID", "OFFERS", Connection()));
        return bk;
    }
    public static String GetPictureUrl(String id)
    {
        return GetUsingId(id, "bookImage", "OFFERS", Connection());
    }
    public static Offer GetOffer(String id)
    {
        Image img = new Image();
        img.ImageUrl = GetPictureUrl(id);
        User usr = new User();
        usr = usr.GetDBData(GetUsingId(id,"userID","OFFERS",Connection())) as User;
        Book bk = new Book();
        bk = bk.GetDBData(GetUsingId(id, "bookID", "OFFERS", Connection())) as Book;
        return new Offer(id,img,float.Parse(GetUsingId(id,"price","OFFERS",Connection())),usr, bk);
    }
    public static List<Offer> GetOffers(String table, String key, String id)
    {
        List<Offer> lst = new List<Offer>();
        DataTable Offers = GetDTFromTowTables("OFFERS", table, key, id);
        foreach(DataRow dr in Offers.Rows)
        {
            Offer offer = new Offer();
            offer.user = new User();
            offer.user.GetFromDB(dr["userID"].ToString());
            offer.id = dr["id"].ToString();
            offer.DataType = "offer";
            offer.price = float.Parse(dr["price"].ToString());
            offer.book = new Book();
            offer.book.GetFromDB(dr["bookID"].ToString());
            offer.picture = new Image();
            offer.picture.ImageUrl = dr["bookImage"].ToString();
            offer.GetImageLocation();
            lst.Add(offer);
        }
        return lst;
    }
    public static void AddOffer(Offer off)
    {
        String bookId = off.book.id;
        String userId = off.user.id;
        String bookImage = off.picture.ImageUrl;
        String price = off.price.ToString();

        SqlCommand command = new SqlCommand();
        command.Connection = Connection();
        command.Parameters.AddWithValue("@book", bookId);
        command.Parameters.AddWithValue("@user", userId);
        command.Parameters.AddWithValue("@price", price);
        command.Parameters.AddWithValue("@picture", bookImage);
        command.CommandText = "INSERT INTO OFFERS (userID,price,bookImage,bookID) VALUES (@user,@price,@picture,@book)";
        command.Connection.Open();
        int rowsAffected = command.ExecuteNonQuery();
        command.Connection.Close();
    }
    public static bool CheckUserId(Offer off,string userId)
    {
        return userId.Equals(GetUsingId(off.id,"userID","OFFERS",Connection()));
    }
    public static void UpdateOffer(Offer off)
    {
        Do("UPDATE OFFERS SET userId='" + off.user.id + "' WHERE id=" + off.id,Connection());
        Do("UPDATE OFFERS SET bookImage='" + off.picture.ImageUrl + "' WHERE id=" + off.id, Connection());
        Do("UPDATE OFFERS SET price='" + off.price + "' WHERE id=" + off.id, Connection());
        Do("UPDATE OFFERS SET bookId='" + off.book.id + "' WHERE id=" + off.id, Connection());
    }
}