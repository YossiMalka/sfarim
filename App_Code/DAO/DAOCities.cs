﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for DAOCities
/// </summary>
public class DAOCities:DAOBasic
{ 
    public DAOCities()
    {
        //
        // TODO: Add constructor logic here
        //

    }

    public static String getId(String cityName)
    {
        try
        {
            DataTable dt;
            dt = Get("select * from CITIES where name=N'" + cityName + "'", Connection());
            return dt.Rows[0]["id"].ToString();
        }
        catch (Exception ex)
        {
            return ex.ToString();
        }
    }
    public static City GetCity(String id)
    {
        return new City("city", id, GetUsingId(id, "name", "CITIES", Connection()));
    }
    public static List<City> GetCities(String table,String key,String id)
    {
        List<City> lst = new List<City>();
        DataTable Offers = GetDTFromTowTables("CITIES", table, key, id);
        foreach (DataRow dr in Offers.Rows)
        {
            City city = new City();
            city.name = dr["name"].ToString();
            city.usersInCity = int.Parse(dr["usersInCity"].ToString());
            city.id = dr["id"].ToString();
            city.DataType = "city";
            lst.Add(city);
        }
        return lst;
    }
    public static void UpdateCity(int id, City toUpdate) {
        
    }

}