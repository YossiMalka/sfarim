﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for DAOBooks
/// </summary>
public class DAOBooks : DAOBasic
{
    public DAOBooks()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public static String GetName(String id)
    {
        return GetUsingId(id, "name", "Books", Connection());
    }
    public static Subject GetSubject(String id)
    {
        Subject sub = new Subject();
        sub.id = GetUsingId(id, "SubjectID", "Books", Connection());
        sub.GetFromDB(sub.id);
        return sub;
    }
    public static String GetAthor(String id)
    {
        return GetUsingId(id, "name", "Books", Connection());
    }
    public static String GetPictureUrl(String id)
    {
        return GetUsingId(id, "name", "Books", Connection());
    }
    public static Book GetBook(String id)
    {
        Book book = new Book();
        book.id = id;
        book.name = GetName(id);
        book.subject = GetSubject(id);
        book.athor = GetAthor(id);
        return book;
    }
    public static List<Book> GetBooks(String table, String key, String id)
    {
        List<Book> lst = new List<Book>();
        DataTable Books = GetDTFromTowTables("Books", table, key, id);
        foreach(DataRow dr in Books.Rows)
        {
            Book book = new Book();
            book.subject = DAOSubjects.GetSubject(dr["subjectID"].ToString());
            book.id = dr["id"].ToString();
            book.DataType = "book";
            book.name = dr["name"].ToString();
            book.athor = dr["athor"].ToString();
            lst.Add(book);
        }
        return lst;
    }
    public static List<Book> GetAllBooks()
    {
        List<Book> lst = new List<Book>();
        DataTable Books = Get("SELECT * FROM Books", Connection());
        foreach (DataRow dr in Books.Rows)
        {
            Book book = new Book();
            book.subject = DAOSubjects.GetSubject(dr["subjectID"].ToString());
            book.id = dr["id"].ToString();
            book.DataType = "book";
            book.name = dr["name"].ToString();
            book.athor = dr["athor"].ToString();
            lst.Add(book);
        }
        return lst;
    }
    public static int CountBooks()
    {
        DataTable Books = Get("SELECT * FROM Books", Connection());
        return Books.Rows.Count;
    }

}