﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;



/// <summary>
/// Summary description for DAOBasic
/// </summary>
public class DAOBasic
{
    protected static String ConnString = GetConnString.connString;
    public DAOBasic()
    {

    }
    public static bool isExist(String key, String value, String Table)
    {
        DataSet ds = new DataSet();
        SqlConnection conn = Connection();
        SqlCommand cmd = new SqlCommand();
        string strsql = "SELECT * FROM users WHERE " + key + "='" + value + "'";
        cmd.CommandText = strsql;
        //      Response.Write(strsql);
        cmd.Connection = conn;
        conn.Open();
        SqlDataAdapter adapter = new SqlDataAdapter(cmd);
        adapter.Fill(ds);
        //שלב שלישי: ההתנתקות ממסד הנתונים 
        conn.Close();
        DataTable dt = ds.Tables[0];
        if (dt.Rows.Count == 0)
        {
            return false;
        }
        return true;
    }
    public static DataRow GetRowUsingId(String table, String id)
    {
        DataSet ds = new DataSet();
        SqlCommand cmd = new SqlCommand();
        SqlConnection conn = Connection();
        cmd.CommandText = "select * from " + table + " where id='" + id + "'";
        cmd.Connection = conn;
        conn.Open();
        SqlDataAdapter adapter = new SqlDataAdapter(cmd);
        adapter.Fill(ds);
        conn.Close();
        DataTable dt = ds.Tables[0];
        DataRow dr = dt.Rows[0];
        return dr;


    }
    public static SqlConnection GetConnection(String connectionString)
    {
        return new SqlConnection(connectionString);
    }
    protected static SqlConnection Connection()
    {
        SqlConnection conn = new SqlConnection(ConnString);
        return conn;
    }
    protected static SqlConnection Connection(bool open)
    {
        SqlConnection conn = new SqlConnection(ConnString);
        if (open)
        {
            conn.Open();
        }
        return conn;
    }
    public static DataTable Get(String SQL, SqlConnection conn)
    {
        DataSet ds = new DataSet();
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = SQL; ;
        cmd.Connection = conn;
        conn.Open();
        SqlDataAdapter adapter = new SqlDataAdapter(cmd);
        adapter.Fill(ds);
        conn.Close();
        return ds.Tables[0];
    }
    public static String GetStr(String SQL, SqlConnection conn)
    {
        DataSet ds = new DataSet();
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = SQL; ;
        cmd.Connection = conn;
        conn.Open();
        SqlDataAdapter adapter = new SqlDataAdapter(cmd);
        adapter.Fill(ds);
        conn.Close();

        DataTable dt = ds.Tables[0];
        DataRow dr = dt.Rows[0];
        return dr.ToString();

    }
    public static String GetUsingId(String id, String param, String table, SqlConnection conn)
    {
        DataSet ds = new DataSet();
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "select " + param + " from " + table + " where id='" + id + "'";
        cmd.Connection = conn;
        conn.Open();
        SqlDataAdapter adapter = new SqlDataAdapter(cmd);
        adapter.Fill(ds);
        //שלב שלישי: ההתנתקות ממסד הנתונים 
        conn.Close();

        DataTable dt = ds.Tables[0];
        DataRow dr = dt.Rows[0];
        return dr[0].ToString();
    }
    public static String GetUsingId(string id, string param, DataTables table)
    {
        string tableStr = "";
        switch (table)
        {
            case DataTables.Books:
                tableStr = "Books";
                break;
            case DataTables.CITIES:
                tableStr = "CITIES";
                break;
            case DataTables.COMMENTS:
                tableStr = "COMMENTS";
                break;
            case DataTables.OFFERS:
                tableStr = "OFFERS";
                break;
            case DataTables.SUBJECTS:
                tableStr = "SUBJECTS";
                break;
            case DataTables.USERS:
                tableStr = "USERS";
                break;
        }
        return GetUsingId(id, param, tableStr, Connection());
    }
    public static DataColumn GetDCUsingId(String id, String param, String table, SqlConnection conn)
    {
        DataSet ds = new DataSet();
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "select " + param + " from " + table + " where id='" + id + "'";
        cmd.Connection = conn;
        conn.Open();
        SqlDataAdapter adapter = new SqlDataAdapter(cmd);
        adapter.Fill(ds);
        //שלב שלישי: ההתנתקות ממסד הנתונים 
        conn.Close();

        DataTable dt = ds.Tables[0];
        DataRow dr = dt.Rows[0];
        return (DataColumn)dr[0];

    }
    public static DataRow GetDRUsingId(String id, String param, String table, SqlConnection conn)
    {
        DataSet ds = new DataSet();
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "select " + param + " from " + table + " where id='" + id + "'";
        cmd.Connection = conn;
        conn.Open();
        SqlDataAdapter adapter = new SqlDataAdapter(cmd);
        adapter.Fill(ds);
        //שלב שלישי: ההתנתקות ממסד הנתונים 
        conn.Close();

        DataTable dt = ds.Tables[0];
        DataRow dr = dt.Rows[0];
        return dr;

    }
    public static bool isIn(String arg, String inArg, String val, String inVal, String table, SqlConnection conn)
    {
        DataSet ds = new DataSet();

        SqlCommand cmd = new SqlCommand();
        string strsql = "SELECT * FROM users WHERE " + inArg + "='" + inVal + "' AND " + arg + "='" + val + "'";
        cmd.CommandText = strsql;
        //      Response.Write(strsql);
        cmd.Connection = conn;
        conn.Open();
        SqlDataAdapter adapter = new SqlDataAdapter(cmd);
        adapter.Fill(ds);
        //שלב שלישי: ההתנתקות ממסד הנתונים 
        conn.Close();
        DataTable dt = ds.Tables[0];
        if (dt.Rows.Count == 0)
        {
            return false;
        }
        return true;


    }
    public static void Insert(String[] key, String[] value)
    {
        /// <summary>
        /// The new insert method.
        /// you don't need to send connection string or sqlConnection.
        /// all what you need to do is to send arry of parameters and arry of values that you whant to add to your parameters.
        /// </summary>
        String parametersString = "";
        String valuesString = "";
        SqlCommand comm = new SqlCommand();
        SqlConnection conn = Connection();
        comm.Connection = conn;
        foreach (String str in key)
        {
            parametersString += key + ",";
        }
        parametersString = parametersString.Remove(parametersString.Length - 1);
        String[] parameters = new String[valuesString.Length];
        foreach (String val in value)
        {
            comm.Parameters.AddWithValue("@" + val, val);
            valuesString += "@" + val + ",";
        }
        valuesString = valuesString.Remove(valuesString.Length - 1);
        String cmd = "INSERT INTO EMAILS (" + parametersString + ") VALUES (" + valuesString + ")";
        comm.CommandText = cmd;
        conn.Open();
        int rowsAffected = comm.ExecuteNonQuery();
        conn.Close();

    }
    public static void Do(String SQL, SqlConnection conn)
    {
        String type = SQL.Split(' ')[0];
        if (type.Equals("delete") || type.Equals("DELETE"))
        {
            Delete(SQL, conn);
        }
        else if (type.Equals("insert") || type.Equals("INSERT"))
        {
            Insert(SQL, conn);
        }
        else if (type.Equals("update") || type.Equals("UPDATE"))
        {
            Update(SQL, conn);
        }
    }
    public static void Remove(String conKey, String conVal)
    {
        SqlCommand comm = new SqlCommand();
        SqlConnection conn = Connection();
        comm.Connection = conn;
        comm.Parameters.AddWithValue("@" + conVal, conVal);
        comm.CommandText = "DELETE FROM EMAILS WHERE " + conKey + "= @" + conKey;
        conn.Open();
        int rowsAffected = comm.ExecuteNonQuery();
        conn.Close();

    }
    private static void Delete(String SQL, SqlConnection conn)
    {
        SqlCommand command = new SqlCommand(SQL, conn);
        conn.Open();
        int rowsAffected = command.ExecuteNonQuery();
        conn.Close();
    }
    private static void Insert(String SQL, SqlConnection conn)
    {
        SqlCommand command = new SqlCommand(SQL, conn);
        conn.Open();
        int rowsAffected = command.ExecuteNonQuery();
        conn.Close();
    }
    private static void Update(String SQL, SqlConnection conn)
    {

        SqlCommand command = new SqlCommand(SQL, conn);
        conn.Open();
        int rowsAffected = command.ExecuteNonQuery();
        conn.Close();
    }
    public static void GetPicture(String id, String key, String table)
    {
        DataRow row = GetDRUsingId(id, key, table, Connection());

    }
    public static String GetFromTowTables(String table1, String table2, String getParam, String key, String setParam, String cloneParam)
    {
        ///
        ///table1 get; table2 set; getParam getId; key setId; setParam equalCondition
        ///
        String SQl = "select " + table1 + "." + getParam + " from " + table1 + " inner join " + table2 + " on " + table2 + "." + key + " = " + table1 + "." + cloneParam + " where " + table2 + "." + key + "=" + setParam;
        SqlConnection conn = Connection(true);
        SqlCommand comm = new SqlCommand(SQl, conn);
        DataSet ds = new DataSet();
        SqlDataAdapter da = new SqlDataAdapter(comm);
        da.Fill(ds);
        conn.Close();
        DataTable dt = ds.Tables[0];
        DataRow dr = dt.Rows[0];
        return dr[0].ToString();

    }
    public static string GrtFromTwoTables(DataTables table1, DataTables table2, String getParam, String key, String setParam, String cloneParem)
    {
        string t1 = Enums.GetFromEnum(table1);
        string t2 = Enums.GetFromEnum(table2);

        return GetFromTowTables(t1, t2, getParam, key, setParam, cloneParem);
    }
    public static DataTable GetDTFromTowTables(String table1, String table2, String key, String cloneParam)
    {
        ///
        ///table1 get; table2 set; key setId; setParam equalCondition
        ///
        String SQl = "select " + /*table1 + "." + getParam*/"*" + " from " + table1 + " inner join " + table2 + " on " + table2 + "." + key + " = " + table1 + "." + cloneParam;
        SqlConnection conn = Connection(true);
        SqlCommand comm = new SqlCommand(SQl, conn);
        DataSet ds = new DataSet();
        SqlDataAdapter da = new SqlDataAdapter(comm);
        da.Fill(ds);
        conn.Close();
        return ds.Tables[0];
        /* DataRow dr = dt.Rows[0];
         return dr[0].ToString();*/

    }
    public static DataTable GetDTFromTowTables(DataTables table1, DataTables table2, String key, String cloneParam)
    {
        string t1 = Enums.GetFromEnum(table1);
        string t2 = Enums.GetFromEnum(table2);

        return GetDTFromTowTables(t2, t1, key, cloneParam);
    }

    public static void UpdateTable(Object toUpdate) {
        String id = ((DataFather)toUpdate).id;
        String type = ((DataFather)toUpdate).DataType;
        foreach (var prop in toUpdate.GetType().GetProperties())
        {
            //prop.GetType
            /*switch (prop.GetType()) {

            }*/
            //Console.WriteLine("{0} = {1}", prop.Name, prop.GetValue(obj, null));
        }
    }

}