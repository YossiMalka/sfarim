﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using FastCoding.userInterface;
using Tools;


/// <summary>
/// Summary description for DAOUser
/// </summary>
public class DAOUser: DAOBasic
{
    
	public DAOUser()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public bool CheckPass(String uName, String pass,String table,String userInTable,String passwordInTable,String connString){
        return isIn( userInTable , passwordInTable, uName, pass,table, Connection());

    }
    public bool CheckPass(String uName, String pass,String connString)
    {
       return isIn("userName","password",uName,pass,"USERS",Connection());
    }
    public bool isMan(String uName,String connString)
    {
        try {
            SqlConnection conn = GetConnection(connString);
            DataTable dt;
            dt = Get("select * from USERS where email='" + uName + "'", conn);
            if ((bool)dt.Rows[0]["isManager"])
                return true;
            return false;
        }
        catch
        {
            return false;
        }
    }
    public String getId(String uName,String connString)
    {
        try
        {
            SqlConnection conn = GetConnection(connString);
            DataTable dt;
            dt = Get("select * from USERS where email='" + uName + "'", conn);
            return dt.Rows[0]["id"].ToString();
        }
        catch (Exception ex)
        {
            return ex.ToString();
        }
    }

    public void deleteUser(String id,String connString)
    {
        SqlConnection conn = GetConnection(connString);
        Do("delete from users where Id=" + id, conn);
    }
   /* public String manTable(String connString)
    {
        SqlConnection conn = GetConnection(connString);
        DataTable dt= Get("select * from users", conn);
        toTable tt = new toTable();
        return tt.dtToTable(dt,0);
    }*/

    public static String getFromTableUsingId(String userId,String arg)
    {
        String connString = GetConnString.connString;
        SqlConnection conn=GetConnection(connString);
        DataSet ds = new DataSet();

        SqlCommand cmd = new SqlCommand();
        string strsql = "SELECT "+arg+" FROM users WHERE  id ='" + userId+"'";
        cmd.CommandText = strsql;
        cmd.Connection = conn;
        conn.Open();
        SqlDataAdapter adapter = new SqlDataAdapter(cmd);
        adapter.Fill(ds);
        conn.Close();
        DataTable dt = ds.Tables[0];

        return dt.Rows[0][0].ToString();
    }
    public void editUser(String id,String[] new1, String connString)
    {
        //[userName,password,address,cash,birthYear,height,userId]
        SqlConnection conn = GetConnection(connString);
        //DataTable dt = Get("select * from users where id = '" + id + "'",conn);

        //String up="";

        for (int i = 0; i < new1.Length; i++) { 
            //String ele="";
            if(!(new1[i]==null||new1[i].Equals(""))){
                switch(i){
                    case 0:
                        Do("update users set userName='" + new1[i] + "' where Id=" + id, conn);
                        
                        break;
                    case 1:
                        Do("update users set password='" + new1[i] + "' where Id=" + id, conn);
                        
                        break;
                    case 2:
                        Do("update users set address='" + new1[i] + "' where Id=" + id, conn);
                        
                        break;
                    case 3:
                        Do("update users set cash='" + new1[i] + "' where Id=" + id, conn);
                        
                        break;
                    case 4:
                        Do("update users set birthYear='" + new1[i] + "' where Id=" + id, conn);
                        
                        break;
                    case 5:
                        Do("update users set height='" + new1[i] + "' where Id=" + id, conn);
                        
                        break;
                    case 6:
                        Do("update users set userId='" + new1[i] + "' where Id=" + id, conn);
                        
                        break;
                    default:
                        
                        break;
                }

                //Do("update users set "+ele +" = '"+new1[i]+ "'where Id=" + id, conn);

            }
        
        }


    }
    public static void addUser(String[] aus)
    {
        String ins ="'"+aus[0];

        for (int i = 1; i < aus.Length; i++)
        {
            ins += "','";
            ins += aus[i];
        }

        ins += "'";
        Do("insert into USERS(email,name,password,isManager,cityID,street,sellsSum,pictureUrl) values(" + ins + ")", Connection());

    }

    public bool checkDropCash(int cash, String id, String connString) {
        SqlConnection conn = GetConnection(connString);
        if (int.Parse(GetUsingId(id, "cash", "users", conn)) >= cash)
        {
          return true;
            
        }
        return false;
        
    }

    public void dropCash(int cash, String id, String connString) {
        SqlConnection conn = GetConnection(connString);
        String current = GetUsingId(id, "cash", "users", conn);
        Do("update users set cash=" + (int.Parse(current) - cash) + " where Id=" + id, conn);
        
    }
    
    public static void GetRowUsingId(String id)
    {
        GetRowUsingId("USERS", id);
    }
    public static User GetUser(String id)
    {
        User ret = new User(id,new System.Web.UI.WebControls.Image(),"","","","",new City(),0f);
        ret.name = GetUsingId(id, "name", "USERS", Connection());
        ret.password = GetUsingId(id, "password", "USERS", Connection());
        ret.profilePicture = new System.Web.UI.WebControls.Image ();
        ret.profilePicture.ImageUrl = GetUsingId(id, "imagePath", "USERS", Connection());
        if (StringChecks.IsFloat(GetUsingId(id, "sellsSum", "USERS", Connection())))
        {
            ret.sellsSum =float.Parse(GetUsingId(id, "sellsSum", "USERS", Connection()));
        }
        else
        {
            ret.sellsSum = 0;
        }
        ret.street = GetUsingId(id, "street", "USERS", Connection());
        ret.Email = GetUsingId(id, "email", "USERS", Connection());
        ret.city = new City();
        return ret;
    }
    public static String GetUserCityName(String id) {
        String cityId = GetUsingId(id, "cityId", "USERS", Connection());
        String cityName = GetFromTowTables("CITIES", "USERS", "name", "cityId", id, "id");
       return cityName;
    }
    public static City GetUserCity(String id)
    {
        return new City("city", GetUsingId(id, "cityId", "USERS",Connection()),GetUserCityName(id));
    }
    public static List<User> GetUsers(String table, String key, String id)
    {
        List<User> lst = new List<User>();
        DataTable Users = GetDTFromTowTables("USERS", table, key, id);
        foreach (DataRow dr in Users.Rows)
        {
            User user = new User();
            user.name = dr["name"].ToString();
            user.id = dr["id"].ToString();
            user.DataType = "user";
            user.Email = dr["email"].ToString();
            user.password = dr["password"].ToString();
            user.profilePicture = new System.Web.UI.WebControls.Image();
            user.profilePicture.ImageUrl = dr["imagePath"].ToString();
            user.street = dr["street"].ToString();
            user.city = DAOUser.GetUserCity(user.id);
            if (StringChecks.IsBool(dr["isManager"]))
            {
                user.isManager = bool.Parse(dr["isManager"].ToString());
            }
            else
            {
                user.isManager = false;
            }
            if (StringChecks.IsFloat(dr["sellsSum"].ToString()))
            {
                user.sellsSum = float.Parse(dr["sellsSum"].ToString());
            }
            else
            {
                user.sellsSum = 0;
            }
            lst.Add(user);
        }
        return lst;
    }

}