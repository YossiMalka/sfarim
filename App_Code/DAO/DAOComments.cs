﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for DAOComments
/// </summary>
public class DAOComments : DAOBasic
{
    public DAOComments() { }

    public static User GetUser(String id)
    {
        User usr = new User();
        usr.GetDBData(GetUsingId(id, "userID", "", Connection()));
        return usr;
    }
    public static Offer GetOffer(String id)
    {
        Offer off = new Offer();
        off.GetDBData(GetUsingId(id, "offerID", "SUBJECTS", Connection()));
        return off;
    }
    public static float GetRank(String id)
    {
        return float.Parse(GetUsingId(id, "rank", DataTables.COMMENTS));
    }
    public static int GetRankingNumber(String id)
    {
        return int.Parse(GetUsingId(id, "rankingNumber", DataTables.COMMENTS));
    }
    public static bool IsBuy(String id)
    {
        return bool.Parse(GetUsingId(id, "isBuy", DataTables.COMMENTS));
    }
    public static String GetText(String id)
    {
        return GetUsingId(id, "text", DataTables.COMMENTS);
    }
    public static Comment GetComment(String id)
    {
        User usr = new User();
        usr = usr.GetDBData(GetUsingId(id, "userID", "OFFERS", Connection())) as User;
        Offer off = new Offer();
        off = off.GetDBData(GetUsingId(id, "bookID", "OFFERS", Connection())) as Offer;
        return new Comment(id, off, usr, GetRankingNumber(id), GetText(id), IsBuy(id), GetRank(id));
    }
    public static List<Comment> GetOffers(DataTables table, String key, String id)
    {
        List<Comment> lst = new List<Comment>();
        DataTable Comments = GetDTFromTowTables(DataTables.COMMENTS, table, key, id);
        foreach (DataRow dr in Comments.Rows)
        {
            Offer off = new Offer();
            User usr = new User();
            off.GetFromDB(dr["id"].ToString());
            usr.GetFromDB(dr["id"].ToString());
            Comment comment = new Comment(dr["id"].ToString(), off, usr, int.Parse(dr["rankingNumber"].ToString()), dr["text"].ToString(), bool.Parse(dr["isBuy"].ToString()), float.Parse(dr["rank"].ToString()));
            lst.Add(comment);
        }
        return lst;
    }
    public static void AddOffer(Comment comm)
    {
        String offerId = comm.offer.id;
        String userId = comm.user.id;

        SqlCommand command = new SqlCommand();
        command.Connection = Connection();
        command.Parameters.AddWithValue("@offer", offerId);
        command.Parameters.AddWithValue("@user", userId);
        command.Parameters.AddWithValue("@rank", comm.rank.ToString());
        command.Parameters.AddWithValue("@rankingNum", comm.rankingNumber.ToString());
        command.Parameters.AddWithValue("@text", comm.text);
        command.Parameters.AddWithValue("@isBuy", comm.isBuy.ToString());
        command.CommandText = "INSERT INTO OFFERS (offerID,userID,text,isBuy,rank,rankingNum) VALUES (@offer,@user,@text,@isBuy,@rank,@rankingNumber)";
        command.Connection.Open();
        int rowsAffected = command.ExecuteNonQuery();
        command.Connection.Close();
    }
    public static bool CheckUserId(Comment comm, string userId)
    {
        return userId.Equals(GetUsingId(comm.id, "userID", "OFFERS", Connection()));
    }
    public static void UpdateComment(Comment comm)
    {
        Do("UPDATE COMMENTS SET userID='" + comm.user.id + "' WHERE id=" + comm.id, Connection());
        Do("UPDATE COMMENTS SET offerID='" + comm.offer.id + "' WHERE id=" + comm.id, Connection());
        Do("UPDATE COMMENTS SET text='" + comm.text + "' WHERE id=" + comm.id, Connection());
        Do("UPDATE COMMENTS SET isBuy='" + comm.isBuy.ToString() + "' WHERE id=" + comm.id, Connection());
        Do("UPDATE COMMENTS SET rank='" + comm.rank.ToString() + "' WHERE id=" + comm.id, Connection());
        Do("UPDATE COMMENTS SET isBuy='" + comm.rankingNumber.ToString() + "' WHERE id=" + comm.id, Connection());
    }
}

