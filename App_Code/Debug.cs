﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for Debug
/// </summary>
public class MyDebug
{
    public static List<String> rows=new List<String>();
    public static void Write(String str)
    {
        rows.Add(str);
    }
    public static void Clear()
    {
        rows.Clear();
    }
    public static  void Print(Label lbl)
    {
        foreach(String str in rows.ToArray())
        {
            lbl.Text += str+"<br /> /n \n";
        }
    }
  
}