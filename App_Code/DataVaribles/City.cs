﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Tools;


/// <summary>
/// Summary description for City
/// </summary>
public class City : DataFather, DataClass
{
    public String name;
    public int usersInCity;

    public City() : base("City")
    {

    }
    public City(String DataType, String id, String name) : base(DataType, id)
    {
        this.name = name;
        this.usersInCity = 0;
    }
    public City(String DataType, String id, String name,int usersInCity) : base(DataType, id)
    {
        this.name = name;
        this.usersInCity = usersInCity;
    }
    public City Copy()
    {
        return new City(this.DataType,this.id,this.name,this.usersInCity);
    }
    public void GetFromDB(String id)
    {
        City temp = DAOCities.GetCity(id);
        this.id = id;
        this.name = temp.name;
        this.usersInCity = temp.usersInCity;
    }
    public void GetFromDB()
    {
        City temp = DAOCities.GetCity(this.id);
        this.name = temp.name;
        this.usersInCity = temp.usersInCity;
    }
    public DataClass GetNewFromDB(String id)
    {
        return DAOCities.GetCity(id) as DataClass;
    }
    public DataFather GetDBData()
    {
        return DAOCities.GetCity(this.id) as DataFather;
    }
    public DataFather GetDBData(String id)
    {
        return DAOCities.GetCity(id);
    }
    public static List<City> GetCities(String table, String key, String id)
    {
        return DAOCities.GetCities(table, key, id);
    }
}