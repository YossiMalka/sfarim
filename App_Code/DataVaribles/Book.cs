﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Tools;


/// <summary>
/// Summary description for Book
/// </summary>
public class Book :DataFather,DataClass
{
    public String name;
    public String athor;
    public Subject subject;
    public String description;

    public Book():base()
    {
       
    }
    public Book(String DataType,String id,String name,String athor,Subject subject):base(DataType,id)
    {
        this.name = name;
        this.athor = athor;
        this.subject = subject;
    }
    public Book Copy()
    {
        return new Book(this.DataType,this.id,this.name,this.athor,this.subject);
    }
    public void GetFromDB(String id) {
        this.name = DAOBooks.GetName(id);
        this.athor = DAOBooks.GetAthor(id);
        this.subject = DAOBooks.GetSubject(id);
    }
    public DataClass GetNewFromDB(String id)
    {
        return DAOBooks.GetBook(id) as DataClass;
    }
    public DataFather GetDBData()
    {
        return DAOBooks.GetBook(this.id) as DataFather;
    }
    public DataFather GetDBData(String id)
    {
        return DAOBooks.GetBook(id);
    }
    public static List<Book> GetBooks(String table,String key,String id)
    {
        return DAOBooks.GetBooks(table, key, id);
    }
    public void InsertData()
    {
        ChangeVals(DAOBooks.GetBook(this.id));
    }
    public void InsertData(String id)
    {
        ChangeVals(DAOBooks.GetBook(id));
    }
    public void ChangeVals(Book book)
    {
        this.athor = book.athor;
        this.DataType = book.DataType;
        this.id = book.id;
        this.name = book.name;
        this.subject = book.subject;
    }
}