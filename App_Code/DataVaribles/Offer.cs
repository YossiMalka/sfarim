﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Tools;


/// <summary>
/// Summary description for Offer
/// </summary>
public class Offer :DataFather,DataClass
{
    public Book book;
    public float price;
    public Image picture;
    public User user;

    public Offer():base("offer")
    {
       
    }
    public Offer(String id,Image picture,float price,User user,Book book):base("offer",id)
    {
        this.picture = picture;
        this.price = price;
        this.user = user;
        this.book = book;
    }
    public Offer Copy()
    {
        return new Offer(this.id,this.picture,this.price,this.user,this.book);
    }
    public void GetFromDB(String id) {
        Offer off = DAOOffers.GetOffer(id);
        this.book = off.book;
        this.price = off.price;
        this.user = off.user;
        this.picture = off.picture;
    }
    public DataClass GetNewFromDB(String id)
    {
        return DAOOffers.GetOffer(id) as DataClass;
    }
    public DataFather GetDBData()
    {
        return DAOOffers.GetOffer(this.id) as DataFather;
    }
    public DataFather GetDBData(String id)
    {
        return DAOOffers.GetOffer(id) as DataFather;
    }
    public static String GetImageLocation(String imageUrl)
    {
        return StringTools.RewriteFrom(imageUrl,@"imeges\uploads\Offers");
    }
    public String GetImageLocation()
    {
         return StringTools.RewriteFrom(this.picture.ImageUrl, @"imeges\uploads\Offers");
    }
    public void SetImageLocation()
    {
        this.picture.ImageUrl = this.GetImageLocation();
    }
    public static List<Offer> GetOffers(String table,String key,String value)
    {
        return DAOOffers.GetOffers(table,"id",key+" AND "+table+".id="+value);
    }
   public bool CheckUserId(string id)
    {
        return DAOOffers.CheckUserId(this,id);
    }
    public static bool CheckUserId(Offer off,string id)
    {
        return DAOOffers.CheckUserId(off,id);
    }
}