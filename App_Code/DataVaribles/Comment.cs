﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Comment
/// </summary>
public class Comment:DataFather
{
    public int rankingNumber=0;
    public Offer offer;
    public User user;
    public bool isBuy;
    public string text;
    public float rank;
    
    public Comment():base("comment"){}
    public Comment(string id) : base("comment",id) { }
    public Comment(string id,Offer offer,User user,int rankingNumber,string text,bool isBuy,float rank) : base("comment",id) {
        this.offer = offer;
        this.user = user;
        this.rankingNumber = rankingNumber;
        this.rank = rank;
        this.isBuy = isBuy;
        this.text = text;
    }
}