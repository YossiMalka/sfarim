﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Tools;


/// <summary>
/// Summary description for User
/// </summary>
public class User : DataFather, DataClass
{
    public String name;
    public String password;
    public Image profilePicture;
    public String Email;
    public String street;
    public City city;
    public float sellsSum;
    public bool isManager;
    public User() : base("User")
    {

    }
    public User(String id, Image profilePicture, String name, String password, String Email,String street,City city,float sellsSum) : base("user", id)
    {
        this.profilePicture = profilePicture;
        this.name = name;
        this.city = city;
        this.sellsSum=sellsSum;
        this.password = password;
        this.Email = Email;
        this.street = street;
        this.isManager = false;
    }
    public User(String id, Image profilePicture, String name, String password, String Email, String street, City city, float sellsSum,bool isManager) : base("user", id)
    {
        this.profilePicture = profilePicture;
        this.name = name;
        this.city = city;
        this.sellsSum = sellsSum;
        this.password = password;
        this.Email = Email;
        this.street = street;
        this.isManager =isManager;
    }
    public User Copy()
    {
        return new User(this.id, this.profilePicture, this.name, this.password, this.Email,this.street,this.city,this.sellsSum);
    }
    public void GetFromDB(String id)
    {

    }
    public DataClass GetNewFromDB(String id)
    {
        return DAOUser.GetUser(id) as DataClass;
    }
    public DataFather GetDBData()
    {
        return DAOUser.GetUser(this.id) as DataFather;
    }
    public DataFather GetDBData(String id)
    {
        return DAOUser.GetUser(id);
    }
    public static String GetImageLocation(String imageUrl)
    {
        return StringTools.RewriteFrom(imageUrl, @"imeges\uploads\Users");
    }
    public void GetImageLocation()
    {
        this.profilePicture.ImageUrl = StringTools.RewriteFrom(this.profilePicture.ImageUrl, @"imeges\uploads\Users");
    }
    public static List<User> GetUsers(String table, String key, String id)
    {
        return DAOUser.GetUsers(table, key, id);
    }
}