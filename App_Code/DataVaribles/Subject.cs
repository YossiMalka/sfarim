﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Subject
/// </summary>
public class Subject : DataFather, DataClass
{
    public string name;
    public Subject(string id, string name) : base("Subject", id)
    {
        this.name = name;
    }
    public Subject() : base("Subject") { }
    public DataFather GetDBData()
    {
        this.GetFromDB(this.id);
        return this;
    }
    public DataFather GetDBData(string id)
    {
        this.GetFromDB(id);
        return this;
    }
    public void GetFromDB(string id)
    {
        Subject sub = DAOSubjects.GetSubject(id);
        this.name = sub.name;
        this.id = sub.id;
    }
    public static Subject GetSubject(string id)
    {
        Subject ret = new Subject();
        ret.GetFromDB(id);
        return ret;
    }
    public DataClass GetNewFromDB(string id)
    {
        return DAOSubjects.GetSubject(id);
    }
}