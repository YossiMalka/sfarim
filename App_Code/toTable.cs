﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI;


/// <summary>
/// Summary description for toTable
/// </summary>
namespace FastCoding
{
    namespace userInterface
    {
        public class DataTables
        {
            public static string dtToHTMLTable(DataTable DT, int border)
            {
                string ret = "<table border=\"" + border + "\" class=\"table table-hover\"";
                DataRow DR;
                DataColumn DC;
                ret += "<tr class=\"success\">";
                for (int i = 0; i < DT.Columns.Count; i++)
                {
                    DC = DT.Columns[i];
                    ret += "<td class=\"success\">" + DC.ColumnName.ToString() + "</td>";
                }
                ret += "<td class=\"success\">delete</td>";
                ret += "<td class=\"success\">edit</td>";
                ret += "</tr>";

                for (int j = 0; j < DT.Rows.Count; j++)
                {
                    ret += "<tr>";


                    for (int i = 0; i < DT.Columns.Count; i++)
                    {
                        ret += "<td>";
                        DR = DT.Rows[j];
                        ret += DR[i].ToString();
                        ret += "</td>";
                    }
                    if (!(bool)DT.Rows[j]["isMAnager"])
                    {
                        ret += "<td><a href=\"c.aspx?cmd=delete&from=man&userId=" + DT.Rows[j]["Id"].ToString() + "\" class=\"btn btn-danger\">delete</a></td>";
                        ret += "<td><button \"type=submit\" name=\"edit\" id=\"edit\" value=\"" + DT.Rows[j]["Id"].ToString() + "\" class=\"btn btn-warning\">edit user</button> </td>";
                    }
                    else
                    {
                        ret += "<td></td><td></td>";
                    }
                    // ret += "<td><a href=\"c.aspx?cmd=edit&from=man&userId=" + DT.Rows[j]["Id"].ToString() + "\" class=\"btn btn-warning\">edit user</a></td>";
                    ret += "</tr>";
                }

                return ret;
            }
            public static string DataTableToHTMLTable(DataTable DT, int border)
            {
                string ret = "<table border=\"" + border + "\" class=\"table table-hover\"";
                DataRow DR;
                DataColumn DC;
                ret += "<tr class=\"success\">";
                for (int i = 0; i < DT.Columns.Count; i++)
                {
                    DC = DT.Columns[i];
                    ret += "<td class=\"success\">" + DC.ColumnName.ToString() + "</td>";
                }
                ret += "<td class=\"success\">delete</td>";
                ret += "<td class=\"success\">edit</td>";
                ret += "</tr>";

                for (int j = 0; j < DT.Rows.Count; j++)
                {
                    ret += "<tr>";


                    for (int i = 0; i < DT.Columns.Count; i++)
                    {
                        ret += "<td>";
                        DR = DT.Rows[j];
                        ret += DR[i].ToString();
                        ret += "</td>";
                    }
                    if (!(bool)DT.Rows[j]["isMAnager"])
                    {
                        ret += "<td><a href=\"c.aspx?cmd=delete&from=man&userId=" + DT.Rows[j]["Id"].ToString() + "\" class=\"btn btn-danger\">delete</a></td>";
                        ret += "<td><button \"type=submit\" name=\"edit\" id=\"edit\" value=\"" + DT.Rows[j]["Id"].ToString() + "\" class=\"btn btn-warning\">edit user</button> </td>";
                    }
                    else
                    {
                        ret += "<td></td><td></td>";
                    }
                    // ret += "<td><a href=\"c.aspx?cmd=edit&from=man&userId=" + DT.Rows[j]["Id"].ToString() + "\" class=\"btn btn-warning\">edit user</a></td>";
                    ret += "</tr>";
                }

                return ret;
            }
        }
        public class AspTables{
            public static Table fromDoubleArrey(object[][] arr)
            {
                TableRow tr;
                TableCell tc;
                Table tab = new Table();
                for (int i = 0; i < arr.Length; i++)
                {
                    tr = new TableRow();
                    for (int j = 0; j < arr[i].Length; i++)
                    {
                        tc = new TableCell();
                        string st = arr[i][j].ToString();
                        Label lb = new Label();
                        lb.Text = st;
                        tc.Controls.Add(lb);
                        tr.Cells.Add(tc);
                    }
                    tab.Rows.Add(tr);
                }
                return tab;
            }
            public static void FromDoubleArreyToAspTable(object[][] arr, Table tab)
            {
                TableRow tr;
                TableCell tc;

                for (int i = 0; i < arr.Length; i++)
                {
                    tr = new TableRow();
                    for (int j = 0; j < arr[i].Length; i++)
                    {
                        tc = new TableCell();
                        string st = arr[i][j].ToString();
                        Label lb = new Label();
                        lb.Text = st;
                        tc.Controls.Add(lb);
                        tr.Cells.Add(tc);
                    }
                    tab.Rows.Add(tr);
                }
            }
            
            public static Table DataTableToAspTable(DataTable DT)
            {
                TableRow tr;
                TableCell tc;
                Table tab = new Table();
                DataRow DR;
                DataColumn DC;

                for (int j = 0; j < DT.Rows.Count; j++)
                {
                    tr = new TableRow();
                    DR = DT.Rows[j];

                    for (int i = 0; i < DT.Columns.Count; i++)
                    {
                        tc = new TableCell();
                        string st = DR[i].ToString();
                        Label lb = new Label();
                        lb.Text = st;
                        tc.Controls.Add(lb);
                        tr.Cells.Add(tc);

                    }
                    tab.Rows.Add(tr);
                }

                return tab;
            }
            /*public static Table FromDataTableToAspTable(DataTable DT)
            {
            }
             public static DataTable FromAspTableToDataTable(Table tab)
            {
            }
            */
        }
    }
}