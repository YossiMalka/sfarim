﻿
public enum DataTables
{
    SUBJECTS,USERS,COMMENTS,OFFERS,CITIES,Books
};
public class Enums
{
    public static string GetFromEnum(DataTables dt)
    {
        switch (dt)
        {
            case DataTables.Books:
                return "Books";
                break;
            case DataTables.CITIES:
                return "CITIES";
                break;
            case DataTables.COMMENTS:
                return "COMMENTS";
                break;
            case DataTables.OFFERS:
                return "OFFERS";
                break;
            case DataTables.SUBJECTS:
                return "SUBJECTS";
                break;
            case DataTables.USERS:
                return "USERS";
                break;
        }
        return "";
    }
}