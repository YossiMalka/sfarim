﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Data.SqlClient;
using System.Data;
using System.Text.RegularExpressions;
using System.Configuration;
using System.IO;

public partial class Login : System.Web.UI.Page
{
    private readonly bool test = true;
    protected void Page_Load(object sender, EventArgs e)
    { 
            sendMesseges();
            setCityList();
    }

    private void sendMesseges()
    {
        DataSet ds = new DataSet();
        ds.ReadXml(MapPath("xml/messeges.xml"));
        DataTable dt = ds.Tables[0];
        
        String messege = Request.QueryString["ERROR"];
        tryAgain.CssClass = "text-danger";
        if (messege == null)
        {
            messege = Request.QueryString["WARNING"];
            tryAgain.CssClass = "text-warning";
        }
        else if (messege == null)
        {
            messege = Request.QueryString["MESSEGE"];
            tryAgain.CssClass = "text-info";
        }
        if (messege!=null) {
            DataRow[] dr = dt.Select("name='" + messege + "'");
            if (dr.Length > 0)
            {
                this.tryAgain.Text = dr[0]["text"].ToString();
            }
        }
        else{
            this.tryAgain.CssClass = "text-danger";
        }
   
    }

    protected void LogIn_Click(object sender, EventArgs e)
    {
        login(this.userName.Text, this.password.Text);
    }
    private void login(String userName,String password) {
        DAOUser du = new DAOUser();
        string cmd = "select * from USERS where email= '" + userName + "' and password='" + password+ "'";
        SqlConnection conn = new SqlConnection(GetConnString.connString);
        SqlDataAdapter da = new SqlDataAdapter(cmd, conn);
        DataSet ds = new DataSet();
        da.Fill(ds);
        DataTable dt = ds.Tables[0];

        if (dt.Rows.Count > 0)
        {
            Session["userID"] = du.getId(userName, GetConnString.connString);
            Session["isManager"] = du.isMan(userName, GetConnString.connString) ? 1 : 0;
            FormsAuthentication.RedirectFromLoginPage("", false);
        }
        else
        {
            this.tryAgain.Text = "שם המשתמש או הסיסמה שגויים";
        }
    }
    protected void SighnIn_Click(object sender, EventArgs e)
    {
        if (checkSighnInVars())
        {
            SetUser();
            login(this.userEmail.Text, this.Password.Text);
        }
    }
    private bool checkSighnInVars()
    {
        if (this.City.Text.Equals(""))
        {
            this.erorr.Text = "יש לבחור עיר";
            this.tryAgain.Text = "יש לבחור עיר";
            return false;
        }
        else if (this.userEmail.Text.Equals(""))
        {
            this.erorr.Text = "יש להזין אי-מייל";
            this.tryAgain.Text = "יש להזין אי-מייל";
            return false;
        }
        else if (this.Name.Text.Equals(""))
        {
            this.erorr.Text = "יש להזין שם ";
            this.tryAgain.Text = "יש להזין שם ";
            return false;
        }
        else if (this.Password.Text.Equals(""))
        {
            this.erorr.Text = "יש להזין סיסמה";
            this.tryAgain.Text = "יש להזין סיסמה";
            return false;
        }
        else if (this.Addres.Text.Equals(""))
        {
            this.erorr.Text = "יש להזין כתובת מלאה";
            this.tryAgain.Text = "יש להזין כתובת מלאה";
            return false;
        }
        else if (!currentEmail(this.userEmail.Text))
        {
            this.erorr.Text = "כתובת האי-מייל שהזנת אינה תקינה";
            this.tryAgain.Text = "כתובת האי-מייל שהזנת אינה תקינה";
            return false;
        }
        else if (!doubleEmail(this.userEmail.Text))
        {
            this.erorr.Text = "כתובת אי-מייל זו כבר בשימוש באתר זה אנא הכנס כתובת אי-מייל אחרת או התחבר דרך טופס ההתחברות.";
            this.tryAgain.Text = "כתובת אי-מייל זו כבר בשימוש באתר זה אנא הכנס כתובת אי-מייל אחרת או התחבר דרך טופס ההתחברות.";
            return false;
        }
        else if (!doubleEmail(this.userEmail.Text))
        {
            this.erorr.Text = "שם משתמש זה נמצא בשימוש אנא הכנס שם משתמש אחר";
            this.tryAgain.Text = "שם משתמש זה נמצא בשימוש אנא הכנס שם משתמש אחר";
            return false;
        }
        else if (!checkPassword(this.Password.Text))
        {
            this.erorr.Text = "";//send the password conditions
            this.tryAgain.Text = "";//send the password conditions
            return false;
        }
        else if (!doubleEmail(this.userEmail.Text))
        {
            this.erorr.Text = "כתובת אי-מייל זו כבר בשימוש באתר זה אנא הכנס כתובת אי-מייל אחרת או התחבר דרך טופס ההתחברות.";
            this.tryAgain.Text = "כתובת אי-מייל זו כבר בשימוש באתר זה אנא הכנס כתובת אי-מייל אחרת או התחבר דרך טופס ההתחברות.";
            return false;
        }
        return true;
    }
    
    public static bool currentEmail(String Email)
    {
        if (Regex.IsMatch(Email, @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$"))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    private bool doubleUser(String userName)
    {
        return !DAOUser.isExist("name",userName,"USERS");
    }
    private bool checkPassword(String pass)
    {

        return true;
    }
    private bool doubleEmail(String userEmail)
    {
        return !DAOUser.isExist("email", userEmail, "USERS");
    }
    private void setCityList()
    {
        String sqlStr = "Select * From CITIES";
        SqlDataAdapter sda = new SqlDataAdapter(sqlStr, GetConnString.connString);
        DataSet ds = new DataSet();
        sda.Fill(ds);
        this.City.DataSource = ds.Tables[0];
        this.City.DataTextField = "name";
        this.City.DataBind();
    }

    private void SetUser()
    {
        SqlConnection conn = DAOBasic.GetConnection(GetConnString.connString);
        String cityId = "";
        String imgPath = Server.MapPath("imeges/uploads/users")+"/"+this.userEmail.Text+"."+this.image.FileName.Split('.')[this.image.FileName.Split('.').Length-1];
        if (this.image.HasFile)
        {
            imgPath = Server.MapPath("imeges/uploads/users") + "/" + this.userEmail.Text + "." + this.image.FileName.Split('.')[this.image.FileName.Split('.').Length - 1];
            this.image.SaveAs(imgPath);
        }
        else
        {
            imgPath = Server.MapPath("~/imeges/uploads/users") + "/default.png";
        }
        cityId = DAOCities.getId(this.City.Text);
        try
        {
            
            conn.Open();
            SqlCommand insertCommand =
            new SqlCommand("Insert into USERS (email,name,password,isManager,cityID,street,sellsSum,priceSum,imagePath) Values ( @Email,@Name,@Password,'0',@City,@Add,'0','0',@Picture)", conn);
            insertCommand.Parameters.Add("@Picture", SqlDbType.NVarChar).Value=imgPath;
            insertCommand.Parameters.Add("@Email", SqlDbType.NVarChar).Value= this.userEmail.Text;
            insertCommand.Parameters.Add("@Password", SqlDbType.NVarChar).Value = this.Password.Text;
            insertCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = this.Name.Text;
            insertCommand.Parameters.Add("@Add", SqlDbType.NVarChar).Value = this.Addres.Text;
            insertCommand.Parameters.Add("@City", SqlDbType.Int).Value = cityId;

            int queryResult = insertCommand.ExecuteNonQuery();
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }
        login(this.userEmail.Text, this.Password.Text);
    }

}