﻿<%@ Page EnableEventValidation="true"  Language="C#" MasterPageFile="~/FirstMaster.master" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" %>
<asp:Content ContentPlaceHolderID="head" runat="server"></asp:Content>

<asp:Content ContentPlaceHolderID="Body" runat="server">
    <!-- start basic HTML-->
    <div class="container" dir="rtl">
        <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-6">
                <div class="form-group-lg">
                    <asp:TextBox ID="userName" CssClass="form-control" runat="server"></asp:TextBox><br />
                    <asp:TextBox ID="password" CssClass="form-control" TextMode="Password" runat="server"></asp:TextBox><br />
                    <asp:Label ID="tryAgain" CssClass="text-danger" runat="server" Text=""></asp:Label><br />
                    <asp:Button ID="LogIn" CssClass="btn btn-lg btn-block btn-primary" runat="server" Text="התחבר" OnClick="LogIn_Click" />
                    <a class="btn btn-lg btn-block btn-warning" href="#sighnIn" data-toggle="modal">אין לך עדיין משמש? הרשם עכשיו!  </a>

                </div>
            </div>
        </div>
    </div>
    <!-- end of basic HTML-->
   
   
</asp:Content>
<asp:Content ContentPlaceHolderID="modals" runat="server">
     
    <!-- modal sighnIn-->
     <div class="modal fade" id="sighnIn" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="form-horizontal">
                    <div class="modal-header">
                        <h4>הירשם</h4>
                    </div>
                    <div class="modal-body">
                       
                        <!-- email -->
                        <div class="row">
                            <div class="col-lg-10">
                                <asp:TextBox ID="userEmail" placeholder="הכנס אימייל כאן" Rows="10" CssClass="form-control" runat="server"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <label for="userEmail" class="col-lg-2 control-label">אי-מייל</label>
                            </div>
                        </div>

                        <!-- password -->
                        <div class="row">
                            <div class="col-lg-10">
                                <asp:TextBox ID="Password" TextMode="Password" placeholder="הכנס סיסמה כאן" Rows="10" CssClass="form-control" runat="server"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <label for="Password" class="col-lg-2 control-label">סיסמה</label>
                            </div>
                        </div>
                         <!-- name -->
                        <div class="row">
                            
                            <div class="col-lg-10">
                                <asp:TextBox ID="Name" placeholder="הכנס שם כאן" Rows="10" CssClass="form-control" runat="server"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <label for="Name" class="col-lg-2 control-label">שם פרטי</label>
                            </div>
                        </div>
                         <!-- city -->
                        <div class="row">
                            
                            <div class="col-lg-10">
                                <asp:dropdownlist ID="City" CssClass="form-control" runat="server"></asp:dropdownlist>
                            </div>
                            <div class="form-group">
                                <label for="City" class="col-lg-2 control-label">עיר מגורים</label>
                            </div>
                        </div>
                         <!-- addres -->
                        <div class="row">
                            <div class="col-lg-10">
                                <asp:TextBox ID="Addres" placeholder="הכנס כתובת מדוייקת כאן" Rows="10" CssClass="form-control" runat="server"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <label for="Addres" class="col-lg-2 control-label">כתובת</label>
                            </div>
                            
                        </div>
                       
                        <!-- name -->
                        <div class="row">
                            <div class="col-lg-10">
                                <asp:FileUpload ID="image" runat="server" />
                            </div>
                            <div class="form-group">
                                <label for="image" class="col-lg-2 control-label">העלה תמונה לכאן (לא חובה)</label>
                            </div>
                            
                        </div>
                    </div>
                    <!-- erorr lable-->
                    <div class="row">
                            <div class="col-lg-12">
                                <asp:Label ID="erorr" CssClass="text-center text-danger" Text="" runat="server" />
                            </div>
                            
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="#" class="btn btn-danger" data-dismiss="modal">Close</a>
                          <asp:Button CssClass="btn btn-primary" ID="SighnIn" runat="server" OnClick="SighnIn_Click" Text="הרשם עכשיו" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
