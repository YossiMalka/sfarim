﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="UserProfile.aspx.cs" Inherits="UserProfile" %>
<asp:Content ContentPlaceHolderID="Head" ID="Header" runat="server"></asp:Content>
<asp:Content ContentPlaceHolderID="Body" ID="PageBody" runat="server">
    <asp:Image ID="ProfilePicture" onmouseover="bigImg(this)" onmouseout="normalImg(this)" CssClass="img-circle" ImageUrl="~/imeges/uploads/users/default.png"  Width="150px" Height="150px" runat="server" /><br />
    <h1><asp:Label ID="Name" runat="server" CssClass="text-center" Text="UserName"></asp:Label></h1><br />
    <h2><asp:Label ID="Email" runat="server" Text="Email"></asp:Label><br /></h2>
    <h2><asp:Label ID="City" runat="server" Text="עיר מגורים:"></asp:Label><br /></h2>
    <h2><asp:label runat="server" ID="Addres" Text="כתובת:"></asp:label><br /></h2>
    <h2><asp:label runat="server" ID="SellsSum" Text="מספר הספרים שנמכרו:"></asp:label><br /></h2>
    <h2><asp:label runat="server" ID="PriceSum" Text="סכום ההכנסות מאתר זה:"></asp:label><br /></h2>
</asp:Content>