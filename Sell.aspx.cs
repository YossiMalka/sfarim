﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using Tools;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

public partial class Sell : System.Web.UI.Page
{
    static readonly double height = 10;
    static readonly double width = 10;
    static int currentPlace = 0;
    public static String CurrentOffers = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userID"] == null)
        {
            Response.Redirect("Login.aspx?ERROR=NO_SESSION");
        }
        if (!Page.IsPostBack)
        {
            GenerateDropDownList();
        }
        GenerateTable();
        SetMaxSize();
    }
    private void GenerateDropDownList()
    {
        String sqlStr = "Select * From Books";
        SqlDataAdapter sda = new SqlDataAdapter(sqlStr, GetConnString.connString);
        DataSet ds = new DataSet();
        sda.Fill(ds);

        this.BookList.DataSource = ds.Tables[0];
        this.BookList.DataTextField = "name";
        this.BookList.DataValueField = "id";
        this.BookList.DataBind();
    }
    protected void UpdateBookList(object sender, EventArgs e)
    {
        if (this.OfferID.Value.Equals("new"))
        {
            AddNewOffer();
        }
        else
        {
            UpdateOffer();
        }
        Response.Redirect(Request.RawUrl);
    }
    private void AddNewOffer()
    {
        if (StringChecks.IsFloat(this.InsertPrice.Text))
        {
            Offer off = new Offer();
            off.book = new Book();
            off.user = new User();
            off.user.id = Session["userID"].ToString();
            off.book.id = this.BookList.SelectedValue.ToString();
            off.price = float.Parse(this.InsertPrice.Text);
            off.picture = new Image();
            if (this.UploadImage.HasFile)
            {
                String epoch = (DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalMilliseconds.ToString();
                UploadImage.SaveAs(MapPath("imeges/uploads/books") + "\\offer" + epoch + "." + UploadImage.FileName.Split('.')[UploadImage.FileName.Split('.').Length - 1]);
                off.picture.ImageUrl = StringTools.RewriteFrom(MapPath("imeges/uploads/books") + "\\offer" + epoch + "." + UploadImage.FileName.Split('.')[UploadImage.FileName.Split('.').Length - 1], @"imeges\uploads\books");

            }
            else
            {
                off.picture.ImageUrl = StringTools.RewriteFrom(MapPath("imeges/uploads/books") + "\\default.jpg", @"imeges\uploads\books");
            }

            DAOOffers.AddOffer(off);

        }
        else
        {
            Response.Redirect(MapPath("Sell.aspx") + "?ERROR=NO_NUMBER");
        }


    }
    private void UpdateOffer()
    {
        if (StringChecks.IsFloat(this.InsertPrice.Text))
        {
            Offer off = new Offer();
            off.book = new Book();
            off.user = new User();
            off.user.id = Session["userID"].ToString();
            off.book.id = this.BookList.SelectedValue.ToString();
            off.price = float.Parse(this.InsertPrice.Text);
            off.picture = new Image();
            off.id = this.OfferID.Value;
            if (off.CheckUserId(Session["userID"].ToString()))
            {
                if (this.UploadImage.HasFile)
                {
                    String epoch = (DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalMilliseconds.ToString();
                    UploadImage.SaveAs(MapPath("imeges/uploads/books") + "\\offer" + epoch + "." + UploadImage.FileName.Split('.')[UploadImage.FileName.Split('.').Length - 1]);
                    off.picture.ImageUrl = StringTools.RewriteFrom(MapPath("imeges/uploads/books") + "\\offer" + epoch + "." + UploadImage.FileName.Split('.')[UploadImage.FileName.Split('.').Length - 1], @"imeges\uploads\books");
                }
                else
                {
                    off.picture.ImageUrl = BookPicture.ImageUrl;
                }
                DAOOffers.UpdateOffer(off);
            }
            else
            {
                Response.Redirect(MapPath("Sell.aspx") + "?ERROR=TRY_TO_INJECT");
            }
        }
        else
        {
            Response.Redirect(MapPath("Sell.aspx") + "?ERROR=NO_NUMBER");
        }

    }
    protected void SetToNewBook(object sender, EventArgs e)
    {

        this.BookPicture.ImageUrl = MapPath("imeges/uploads/books/default.jpg");
        this.InsertPrice.Text = "";
        this.OfferID.Value = "new";
    }
    private List<Offer[]> GetLstArr(List<Offer> offers)
    {
        int count = 0;
        List<Offer[]> off = new List<Offer[]>();
        while (count < offers.Count)
        {
            Offer[] arr = new Offer[5];
            for (int i = 0; i < 3; i++)
            {
                if (count < offers.Count)
                {
                    arr[i] = offers[count];
                }
                count++;
            }
            off.Add(arr);
        }
        return off;
    }
    private LinkButton GenerateListParameter(String id, String bookName, String imgUrl)
    {
        LinkButton btn = new LinkButton();
        btn.ID = id;
        btn.Click += SetBook;
        HtmlImage img = new HtmlImage();
        img.Src = imgUrl;
        img.Height = 100;
        img.Width = 100;
        Label lbl = new Label();
        lbl.Text = bookName;
        btn.Controls.Add(img);
        btn.Controls.Add(new LiteralControl("<span style=\"font-size:150%;\"><strong>"));
        btn.Controls.Add(lbl);
        btn.Controls.Add(new LiteralControl("</strong></span>"));
        return btn;
    }
    private void SetBook(object sender, EventArgs e)
    {
        Offer offer = new Offer();
        LinkButton lk = sender as LinkButton;
        offer.GetFromDB(lk.ID);
        this.BookPicture.ImageUrl = offer.picture.ImageUrl;
        this.InsertPrice.Text = offer.price.ToString();
        this.OfferID.Value = lk.ID;
        BookList.ClearSelection();
        BookList.Items.FindByValue(offer.book.id).Selected = true;
    }
    private void GenerateTable()
    {
        List<Offer[]> offers = GetLstArr(Offer.GetOffers("USERS", "userID", Session["userID"].ToString()));
        foreach (Offer[] offerArr in offers)
        {
            TableRow tr = new TableRow();
            foreach (Offer offer in offerArr)
            {
                if (offer != null)
                {
                    TableCell tc = new TableCell();
                    tc.Controls.Add(GenerateListParameter(offer.id, offer.book.name, offer.picture.ImageUrl));
                    tr.Controls.Add(tc);
                }
            }
            this.MoreBooks.Controls.Add(tr);
        }
    }
    private void SetMaxSize()
    {
        if (height < this.BookPicture.Height.Value)
        {
            this.BookPicture.CssClass += "heightResize";
        }
        if (height < this.BookPicture.Width.Value)
        {
            this.BookPicture.CssClass += "widthResize";
        }
    }
    protected void Filter_TextChanged(object sender, EventArgs e)
    {
        int removed = 0;
        for (int i = 0; i < this.BookList.Items.Count; i++)
        {
            ListItem item = this.BookList.Items[i];
            this.BookList.Items.Remove(item);
        }
        GenerateDropDownList();
        for (int i = 0; i < this.BookList.Items.Count; i++)
        {
            ListItem item = this.BookList.Items[i - removed];
            if (!Regex.IsMatch(item.Text, this.Filter.Text, RegexOptions.IgnoreCase)/*item.Text.IndexOf(this.Filter.Text) == -1*/)
            {
                this.BookList.Items.RemoveAt(i - removed);
                removed++;
            }
        }

    }

}