﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using Tools;

public partial class UserProfile  : System.Web.UI.Page
{
    string defaultImage= @"imeges/uploads/users/default.png";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userID"] == null || !StringChecks.IsInt(Session["userID"]))
        {
            Response.Redirect("Login.aspx?ERROR=NO_SESSION");
        }
        loadUserData();
    }
    private void loadUserData()
    {
        String userId = Request.Form["user"]!=null?Request.Form["user"]:Session["userID"] != null ? Session["userID"].ToString() : "";
        String city = DAOBasic.GetFromTowTables("CITIES", "USERS", "name", "CityID", DAOBasic.GetUsingId(Session["userID"].ToString(),"cityID", "USERS", DAOBasic.GetConnection(GetConnString.connString)),"id");
        loadUserImage(userId);
        this.Name.Text = DAOUser.GetUsingId(Session["userID"].ToString(), "name", "USERS", DAOBasic.GetConnection(GetConnString.connString));
        this.Email.Text = DAOUser.GetUsingId(Session["userID"].ToString(), "email", "USERS", DAOBasic.GetConnection(GetConnString.connString));
        this.Addres.Text += DAOUser.GetUsingId(Session["userID"].ToString(), "street", "USERS", DAOBasic.GetConnection(GetConnString.connString));
        this.City.Text += city;
        SellsSum.Text+="ספרים"+ DAOUser.GetUsingId(Session["userID"].ToString(), "sellsSum", "USERS", DAOBasic.GetConnection(GetConnString.connString));
        this.PriceSum.Text += "₪"+DAOUser.GetUsingId(Session["userID"].ToString(), "priceSum", "USERS", DAOBasic.GetConnection(GetConnString.connString));
    }
    private void loadUserImage(String id)
    {
        String path=DAOUser.GetUsingId(id, "imagePath", "USERS", DAOUser.GetConnection(GetConnString.connString));
        this.ProfilePicture.ImageUrl = StringTools.RewriteFrom(path, @"imeges/uploads/users");     
    }

}