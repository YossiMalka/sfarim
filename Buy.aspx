﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.master" CodeFile="Buy.aspx.cs" Inherits="Buy" %>
<asp:Content ContentPlaceHolderID="Head" ID="Header" runat="server"></asp:Content>
<asp:Content ContentPlaceHolderID="Body" ID="PageBody" runat="server">
    <asp:PlaceHolder ID="book" runat="server" Visible="false">
    <asp:Image ID="BookPicture"  Width="50ex" Height="50ex" CssClass="img-thumbnail" ImageUrl="~/imeges/uploads/books/default.jpg" runat="server"></asp:Image>
    <br />
    <asp:HiddenField ID="OfferID" runat="server" Value="new" />
    <asp:Table ID="Propeties" runat="server" CssClass="table table-bordered">
        <asp:TableRow runat="server">
            <asp:TableCell runat="server" CssClass="text-center">
                <asp:Label Text="מחבר" CssClass="text-center" runat="server"></asp:Label>
            </asp:TableCell>
            <asp:TableCell runat="server">
                <asp:Label runat="server"></asp:Label>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow runat="server">
            <asp:TableCell ColumnSpan="1" ID="TableCell1" runat="server">
                <asp:Label runat="server" Text="שם הספר"></asp:Label>
            </asp:TableCell>
            <asp:TableCell runat="server">
                <asp:Label ID="BookName" runat="server"></asp:Label>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="BookPrice" runat="server">
            <asp:TableCell ID="PriceText" CssClass="text-center text-uppercase" runat="server"><strong>מחיר</strong></asp:TableCell>
            <asp:TableCell ID="PriceValue" runat="server">
                <asp:Label ID="Price" CssClass="text-center" runat="server"></asp:Label>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="GetBook" runat="server">
            <asp:TableCell runat="server">
                <asp:Button ID="buyBook" runat="server" CssClass="btn btn-lf btn-block btn-info" Text="קנה את הספר" />
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    </asp:PlaceHolder>

    <table class="table">
        <tr>
            <td>
               <asp:CheckBox ID="JustMyCity" Text="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; חפש ספרים רק בעיר שלי &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" Checked="true" CssClass="checkbox" runat="server" />
            </td>
            <td>
                <asp:TextBox ID="Filter" CssClass="form-control form-group-lg" OnTextChanged="Filter_TextChanged" placeholder="הכנס חלק משם הספר כדי לסנן את רשימת הספרים" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:DropDownList ClientIDMode="Static"  AutoPostBack="true" ID="BookList" CssClass="form-control" runat="server" OnSelectedIndexChanged="BookList_SelectedIndexChanged"></asp:DropDownList>
            </td>
        </tr>
    </table>
    <asp:Table ID="MoreBooks" runat="server" CssClass="table table-bordered">
        <asp:TableRow ID="TableTitle" runat="server">
            <asp:TableCell ID="TitleTable" runat="server" ColumnSpan="3"><strong> הצעות מתאימות</strong></asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
<asp:Content ContentPlaceHolderID="modals" runat="server">
     <!-- modal contact-->
     <div class="modal fade" id="sighnIn" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="form-horizontal">
                    <div class="modal-header">
                        <h4>הירשם</h4>
                    </div>
                    <div class="modal-body">
                       <!-- picture -->
                        <div class="text-center">
                            <asp:Image ID="UserPicture" runat="server" ImageAlign="Top" CssClass="img-circle" />
                        </div>
                        <!-- email -->
                        <div class="row">
                            <div class="col-lg-10">
                                <asp:Label ID="UserEmail" runat="server"></asp:Label>
                            </div>
                            <div class="form-group">
                                <label for="userEmail" class="col-lg-2 control-label">אי-מייל</label>
                            </div>
                        </div>
                         <!-- name -->
                        <div class="row">
                            
                            <div class="col-lg-10">
                                <asp:Label ID="UserName" runat="server"></asp:Label>
                            </div>
                            <div class="form-group">
                                <label for="Name" class="col-lg-2 control-label">שם פרטי</label>
                            </div>
                        </div>
                         <!-- city -->
                        <div class="row">
                            
                            <div class="col-lg-10">
                                <asp:Label ID="UserCity" runat="server"></asp:Label>
                            </div>
                            <div class="form-group">
                                <label for="City" class="col-lg-2 control-label">עיר מגורים</label>
                            </div>
                        </div>
                         <!-- addres -->
                        <div class="row">
                            <div class="col-lg-10">
                                <asp:Label ID="UserAddres" runat="server"></asp:Label>
                            </div>
                            <div class="form-group">
                                <label for="Addres" class="col-lg-2 control-label">כתובת</label>
                            </div>
                            
                        </div>
                    </div>
                    <!-- erorr lable-->
                    <div class="row">
                            <div class="col-lg-12">
                                <asp:Label ID="erorr" CssClass="text-center text-danger" Text="" runat="server" />
                            </div>
                            
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="#" class="btn btn-danger" data-dismiss="modal">סגור</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>