﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;



public partial class Buy : System.Web.UI.Page
{
    bool find = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            GenerateDropDownList();
        }
    }
    private void GenerateDropDownList()
    {
        String sqlStr = "Select * From Books";
        SqlDataAdapter sda = new SqlDataAdapter(sqlStr, GetConnString.connString);
        DataSet ds = new DataSet();
        sda.Fill(ds);
        this.BookList.DataSource = ds.Tables[0];
        this.BookList.DataTextField = "name";
        this.BookList.DataValueField = "id";
        this.BookList.DataBind();
    }
    protected void Filter_TextChanged(object sender,EventArgs e)
    {
        int removed = 0;
        for (int i = 0; i < this.BookList.Items.Count; i++)
        {
            ListItem item = this.BookList.Items[i];
            this.BookList.Items.Remove(item);
        }
        GenerateDropDownList();
        for (int i = 0; i < this.BookList.Items.Count; i++)
        {
            ListItem item = this.BookList.Items[i - removed];
            if (!Regex.IsMatch(item.Text, this.Filter.Text, RegexOptions.IgnoreCase)/*item.Text.IndexOf(this.Filter.Text) == -1*/)
            {
                this.BookList.Items.RemoveAt(i - removed);
                removed++;
            }
        }
    }
    protected void BookList_SelectedIndexChanged(object sender, EventArgs e)
    {
        GenerateTable();
    }
    private List<Offer> Search(string bookId,string userCity)
    {
        List<Offer> listWithCity = Search(bookId);
        List<Offer> ret = new List<Offer>();
        foreach (Offer offer in listWithCity)
        {
            if (offer.user.city.Equals(userCity))
            {
                ret.Add(offer);
            }
        }
        return ret;
    }
    private List<Offer> Search(string bookId)
    {
        List<Offer> ret = Offer.GetOffers("Books", "bookID", bookId);
        return ret;
    }
    private LinkButton GenerateListParameter(String id, String bookName, String imgUrl)
    {
        LinkButton btn = new LinkButton();
        btn.ID = id;
        btn.Click += SetBook;
        HtmlImage img = new HtmlImage();
        img.Src = imgUrl;
        img.Height = 100;
        img.Width = 100;
        Label lbl = new Label();
        lbl.Text = bookName;
        btn.Controls.Add(img);
        btn.Controls.Add(new LiteralControl("<span style=\"font-size:150%;\"><strong>"));
        btn.Controls.Add(lbl);
        btn.Controls.Add(new LiteralControl("</strong></span>"));
        return btn;
    }
    protected void SetBook(object sender,EventArgs e)
    {
        Response.Write("<script type=\"text\\javascript\">window.alert(\"setBook\")<\\script>");
    }
    private void GenerateTable()
    {
        
        List<Offer[]> offers;
        if (this.JustMyCity.Checked)
        {
            City userCity = DAOUser.GetUserCity(Session["userID"].ToString());
            offers = GetLstArr(Search(this.BookList.SelectedValue.ToString(),userCity.id));
        }
        else
        {
            offers= GetLstArr(Search(this.BookList.SelectedValue.ToString()));
        }
        foreach (Offer[] offerArr in offers)
        {
            TableRow tr = new TableRow();
            foreach (Offer offer in offerArr)
            {
                if (offer != null)
                {
                    TableCell tc = new TableCell();
                    tc.Controls.Add(GenerateListParameter(offer.id, offer.book.name, offer.picture.ImageUrl));
                    tr.Controls.Add(tc);
                }
            }
            this.MoreBooks.Controls.Add(tr);
        }
    }
    private List<Offer[]> GetLstArr(List<Offer> offers)
    {
        int count = 0;
        List<Offer[]> off = new List<Offer[]>();
        while (count < offers.Count)
        {
            Offer[] arr = new Offer[3];
            for (int i = 0; i < 3; i++)
            {
                if (count < offers.Count)
                {
                    arr[i] = offers[count];
                }
                count++;
            }
            off.Add(arr);
        }
        return off;
    }
}