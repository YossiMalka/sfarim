﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.master" CodeFile="default.aspx.cs" Inherits="Home" %>
<asp:Content ContentPlaceHolderID="Head" ID="Header" runat="server"></asp:Content>
<asp:Content ContentPlaceHolderID="Body" ID="PageBody" runat="server">
    <div class="container">
        <div class="row">
            <div class="col-lg-2"></div>
            <div class="col-lg-8">
                <div class="jumbotron">
                    <h1 class="text-center">ברוך הבא</h1>
                    <p class="text-right" dir="rtl">
                        ברוך הבא לאתר היד שניה של ספרי הלימוד. האתר נועד לעזור לכם להשיג ולמכור ספרי לימוד במחיר שמשתלם לכם ולא במחירים שחנויות הספרים מציעות לכם.
                    </p>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6">
                                <asp:HyperLink ID="BuyLink" CssClass="btn btn-success btn-block btn-lg" NavigateUrl="~/Buy.aspx" runat="server">חפש ספרים</asp:HyperLink>
                            </div>
                            <div class="col-lg-6">
                                <asp:HyperLink ID="SellLink" CssClass="btn btn-info btn-block btn-lg" NavigateUrl="~/Buy.aspx" runat="server">הצע ספרים</asp:HyperLink>
                            </div>
                            <asp:image runat="server"></asp:image>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</asp:Content>