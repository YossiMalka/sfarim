﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.master" CodeFile="Sell.aspx.cs" Inherits="Sell" %>

<asp:Content ContentPlaceHolderID="Head" ID="Header" runat="server">
</asp:Content>
<asp:Content ContentPlaceHolderID="Body" ID="PageBody" runat="server">
    <asp:image id="BookPicture" width="50ex" height="50ex" cssclass="img-thumbnail" imageurl="~/imeges/uploads/books/default.jpg" runat="server"></asp:image>
    <asp:fileupload id="UploadImage" runat="server"></asp:fileupload>
    <br />
    <asp:hiddenfield id="OfferID" runat="server" value="new" />
    <asp:table id="Propeties" runat="server" cssclass="table table-bordered">
        <asp:TableRow runat="server">
            <asp:TableCell runat="server" CssClass="text-center">
                <strong>כתוב כאן חלק משם הספר על מנת לסנן ערכים ברשימה</strong>
            </asp:TableCell>
            <asp:TableCell runat="server">
                <asp:TextBox CssClass="form-control" AutoPostBack="true" OnTextChanged="Filter_TextChanged" ID="Filter" runat="server"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>

        <asp:TableRow ID="BookName" runat="server">
            <asp:TableCell ColumnSpan="2" ID="TableCell1" runat="server">
                <asp:DropDownList ClientIDMode="Static" AutoPostBack="true"  ID="BookList" CssClass="form-control" runat="server"></asp:DropDownList>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="BookPrice" runat="server">
            <asp:TableCell ID="PriceText" CssClass="text-center text-uppercase" runat="server"><strong>מחיר</strong></asp:TableCell>
            <asp:TableCell ID="PriceValue" runat="server">
                <asp:TextBox ID="InsertPrice" CssClass="form-control" runat="server"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
    </asp:table>
    <table class="table">
        <tr>
            <td>
                <asp:button id="ChangeBook" onclick="UpdateBookList" cssclass="btn btn-success btn-lg btn-block" runat="server" text="עדכן רשימת הצעות" />
            </td>
            <td>
                <asp:button id="NewBook" onclick="SetToNewBook" cssclass="btn btn-warning btn-lg btn-block" runat="server" text="ספר חדש" />
            </td>
        </tr>
    </table>


    <asp:table id="MoreBooks" runat="server" cssclass="table table-bordered">
        <asp:TableRow ID="TableTitle" runat="server">
            <asp:TableCell ID="TitleTable" runat="server" ColumnSpan="3"><strong> ההצעות שלך</strong></asp:TableCell>
        </asp:TableRow>
    </asp:table>
</asp:Content>
<asp:Content ContentPlaceHolderID="Side" ID="PageSide" runat="server">
    <div class="list-group ">

        <asp:placeholder id="ListHolder" runat="server"></asp:placeholder>
    </div>
</asp:Content>
